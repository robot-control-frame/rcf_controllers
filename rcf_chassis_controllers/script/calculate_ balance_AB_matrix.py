import numpy as np

if __name__ == '__main__':
    M = 5.0
    m = 0.5
    L = 0.15
    I_omega = 0.0025
    I_theta = 0.216667
    I_delta = 0.133333
    R = 0.1
    D = 0.44
    g = 9.81

    X = (
                2 * I_theta * R * R + 2 * L * L * M * R * R) * m + I_theta * M * R * R + 2 * I_theta * I_omega + 2 * M * L * L * I_omega

    a_13 = -(1 / X) * R * R * M * M * L * L * g
    a_43 = (1 / X) * (M * R * R + 2 * m * R * R + 2 * I_omega) * M * g * L
    b_11 = (1 / X) * (I_theta + M * R * L + L * L * M) * R
    b_12 = (1 / X) * (I_theta + M * R * L + L * L * M) * R
    b_21 = -(D * R) / (2 * I_delta * R * R + D * D * I_omega + D * D * m * R * R)
    b_22 = (D * R) / (2 * I_delta * R * R + D * D * I_omega + D * D * m * R * R)
    b_41 = -(1 / X) * (M * R * R + 2 * m * R * R + 2 * I_omega + M * R * L)
    b_42 = -(1 / X) * (M * R * R + 2 * m * R * R + 2 * I_omega + M * R * L)

    A = np.zeros((4, 4))
    A[0][2] = a_13
    A[3][2] = a_43
    B = np.zeros((4, 2))
    B[0][0] = b_11
    B[0][1] = b_12
    B[1][0] = b_21
    B[1][1] = b_22
    B[3][0] = b_41
    B[3][1] = b_42

    print(A)
    print(B)
