/*******************************************************************************
 * Copyright (c) 2023/07/3, Zhou Qi.
 * All rights reserved.
 *******************************************************************************/

#include "rcf_chassis_controllers/balance_mpc.h"

#include <rcf_msgs/BalanceState.h>

#include <pluginlib/class_list_macros.hpp>

#include "rcf_common/ori_tool.h"

namespace rcf_chassis_controllers {
bool BalanceMpcController::init(hardware_interface::RobotHW *robot_hw, ros::NodeHandle &root_nh,
                                ros::NodeHandle &controller_nh) {
  ChassisBase::init(robot_hw, root_nh, controller_nh);
  if (!controller_nh.getParam("wheel_radius", wheel_radius_) || !controller_nh.getParam("wheel_track", wheel_track_)) {
    ROS_ERROR("Some BalanceLqrController params doesn't given (namespace: %s)", controller_nh.getNamespace().c_str());
    return false;
  }
  joint_left_ =
      effort_joint_interface_->getHandle(controller_nh.param("joint_left_name", std::string("left_wheel_joint")));
  joint_right_ =
      effort_joint_interface_->getHandle(controller_nh.param("joint_right_name", std::string("right_wheel_joint")));
  joint_handles_.push_back(joint_left_);
  joint_handles_.push_back(joint_right_);
  hardware_interface::ImuSensorInterface *imu_sensor_interface =
      robot_hw->get<hardware_interface::ImuSensorInterface>();
  std::string imu_name{"base_imu"};
  base_imu_ = imu_sensor_interface->getHandle(imu_name);

  com_pitch_offset_ = controller_nh.param("com_pitch_offset", 0.0);
  XmlRpc::XmlRpcValue a, b, q, r;
  controller_nh.getParam("a", a);
  controller_nh.getParam("b", b);
  controller_nh.getParam("q", q);
  controller_nh.getParam("r", r);
  // mpc slover init
  mpc_solver_ = std::make_unique<Mpc>();
  initSolver(a, b, q, r);
  // ros
  state_real_pub_ = root_nh.advertise<rcf_msgs::BalanceState>("state_real", 100);
  return true;
}

void BalanceMpcController::update(const ros::Time &time, const ros::Duration &period) {
  ChassisBase::update(time, period);
  rcf_msgs::BalanceState state_real_msg;
  state_real_msg.header.stamp = time;
  state_real_msg.x_dot = x_(0);
  state_real_msg.delta_dot = x_(1);
  state_real_msg.theta = x_(2);
  state_real_msg.theta_dot = x_(3);
  state_real_msg.c_l = joint_left_.getEffort();
  state_real_msg.c_r = joint_right_.getEffort();
  state_real_pub_.publish(state_real_msg);
}

void BalanceMpcController::moveJoint(const ros::Time &time, const ros::Duration &period) {
  geometry_msgs::Quaternion quat;
  quat.x = base_imu_.getOrientation()[0];
  quat.y = base_imu_.getOrientation()[1];
  quat.z = base_imu_.getOrientation()[2];
  quat.w = base_imu_.getOrientation()[3];
  double roll{}, pitch{}, yaw{}, pitch_dot{};
  rcf_common::quatToRPY(quat, roll, pitch, yaw);
  pitch_dot = base_imu_.getAngularVelocity()[1];
  // x_
  double joint_left_position = joint_left_.getPosition();
  double joint_right_position = joint_right_.getPosition();
  x_(0) = ((joint_left_position + joint_right_position) / 2) * wheel_radius_;            // x
  x_(1) = odometry().linear.x;                                                           // x_dot
  x_(2) = (-joint_left_position + joint_right_position) * wheel_radius_ / wheel_track_;  // 旋转角度
  x_(3) = odometry().angular.z;                                                          // 旋转角速度
  x_(4) = pitch + com_pitch_offset_;                                                     // 倾角
  x_(5) = pitch_dot;                                                                     // 倾角角速度
  // x_ref_
  x_ref_(0) += vel_cmd_.x * period.toSec();
  x_ref_(1) = vel_cmd_.x;  // x_ref_dot
  x_ref_(2) += vel_cmd_.z * period.toSec();
  x_ref_(3) = vel_cmd_.z;  // 角速度
  // set Status
  mpc_solver_->setStatus(x_, x_ref_);
  // get U
  u_ = mpc_solver_->getControl();
  joint_left_.setCommand(u_(0));
  joint_right_.setCommand(u_(1));
}

geometry_msgs::Twist BalanceMpcController::odometry() {
  geometry_msgs::Twist vel_data;
  double joint_left_velocity = joint_left_.getVelocity();
  double joint_right_velocity = joint_right_.getVelocity();
  vel_data.linear.x = ((joint_left_velocity + joint_right_velocity) / 2) * wheel_radius_;
  vel_data.angular.z = (-joint_left_velocity + joint_right_velocity) * wheel_radius_ / wheel_track_;
  return vel_data;
}

void BalanceMpcController::initSolver(XmlRpc::XmlRpcValue a, XmlRpc::XmlRpcValue b, XmlRpc::XmlRpcValue q,
                                      XmlRpc::XmlRpcValue r) {
  // Check a and q
  ROS_ASSERT(a.getType() == XmlRpc::XmlRpcValue::TypeArray);
  ROS_ASSERT(q.getType() == XmlRpc::XmlRpcValue::TypeArray);
  ROS_ASSERT(a.size() == STATE_DIM);
  ROS_ASSERT(q.size() == STATE_DIM);
  for (int i = 0; i < STATE_DIM; ++i) {
    ROS_ASSERT(a[i].getType() == XmlRpc::XmlRpcValue::TypeArray);
    ROS_ASSERT(q[i].getType() == XmlRpc::XmlRpcValue::TypeArray);
    ROS_ASSERT(a[i].size() == STATE_DIM);
    ROS_ASSERT(q[i].size() == STATE_DIM);
    for (int j = 0; j < STATE_DIM; ++j) {
      ROS_ASSERT(a[i][j].getType() == XmlRpc::XmlRpcValue::TypeDouble ||
                 a[i][j].getType() == XmlRpc::XmlRpcValue::TypeInt);
      ROS_ASSERT(q[i][j].getType() == XmlRpc::XmlRpcValue::TypeDouble ||
                 q[i][j].getType() == XmlRpc::XmlRpcValue::TypeInt);
      if (a[i][j].getType() == XmlRpc::XmlRpcValue::TypeDouble)
        a_(i, j) = static_cast<double>(a[i][j]);
      else if (a[i][j].getType() == XmlRpc::XmlRpcValue::TypeInt)
        a_(i, j) = static_cast<int>(a[i][j]);
      if (q[i][j].getType() == XmlRpc::XmlRpcValue::TypeDouble)
        q_(i, j) = static_cast<double>(q[i][j]);
      else if (q[i][j].getType() == XmlRpc::XmlRpcValue::TypeInt)
        q_(i, j) = static_cast<int>(q[i][j]);
    }
  }
  // Check b
  ROS_ASSERT(b.getType() == XmlRpc::XmlRpcValue::TypeArray);
  ROS_ASSERT(b.size() == STATE_DIM);
  for (int i = 0; i < STATE_DIM; ++i) {
    ROS_ASSERT(b[i].getType() == XmlRpc::XmlRpcValue::TypeArray);
    ROS_ASSERT(b[i].size() == CONTROL_DIM);
    for (int j = 0; j < CONTROL_DIM; ++j) {
      ROS_ASSERT(b[i][j].getType() == XmlRpc::XmlRpcValue::TypeDouble ||
                 b[i][j].getType() == XmlRpc::XmlRpcValue::TypeInt);
      if (b[i][j].getType() == XmlRpc::XmlRpcValue::TypeDouble)
        b_(i, j) = static_cast<double>(b[i][j]);
      else if (b[i][j].getType() == XmlRpc::XmlRpcValue::TypeInt)
        b_(i, j) = static_cast<int>(b[i][j]);
    }
  }
  // Check r
  ROS_ASSERT(r.getType() == XmlRpc::XmlRpcValue::TypeArray);
  ROS_ASSERT(r.size() == CONTROL_DIM);
  for (int i = 0; i < CONTROL_DIM; ++i) {
    ROS_ASSERT(r[i].getType() == XmlRpc::XmlRpcValue::TypeArray);
    ROS_ASSERT(r[i].size() == CONTROL_DIM);
    for (int j = 0; j < CONTROL_DIM; ++j) {
      ROS_ASSERT(r[i][j].getType() == XmlRpc::XmlRpcValue::TypeDouble ||
                 r[i][j].getType() == XmlRpc::XmlRpcValue::TypeInt);
      if (r[i][j].getType() == XmlRpc::XmlRpcValue::TypeDouble)
        r_(i, j) = static_cast<double>(r[i][j]);
      else if (r[i][j].getType() == XmlRpc::XmlRpcValue::TypeInt)
        r_(i, j) = static_cast<int>(r[i][j]);
    }
  }

  // init mpc solver
  Eigen::Matrix<double, 2, 1> tor_constraint;
  tor_constraint << 12, 12;
  int qp_horizon = 10;
  mpc_solver_->init(a_, b_, q_, r_, tor_constraint, qp_horizon);
}

}  // namespace rcf_chassis_controllers

PLUGINLIB_EXPORT_CLASS(rcf_chassis_controllers::BalanceMpcController, controller_interface::ControllerBase)
